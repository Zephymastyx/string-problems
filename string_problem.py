import functions
import pdb
from abc import ABCMeta, abstractmethod

class Sequence:
    def __init__(self, str, changes, pos, edit_sequence, lower_bound):
        """
        initializes Sequence object
        str:            string of the sequence
        changes:        number of edit operations already performed on sequence
        pos:            position in the original string
        edit_sequence:  operations to reach current string from original string
        lower_bound:    Info on lower bound for edit operations to use
        """
        self.string = str
        self.cost = changes
        self.last_operation = None
        self.previous_character = None
        self.pos = pos
        self.edit_sequence = edit_sequence
        self.lower_bound = lower_bound
        self.deleted_string = None

    def __repr__(self):
        return "pos " + str(self.pos) + " " + self.string[:20] + " " + str(self.cost) + " changes, " + str(self.lower_bound["value"]) + " min changes"

class StringProblemInstance:
    __metaclass__ = ABCMeta

    def __init__(self, sequences, d):
        """
        initializes String Problem instance
        sequences:  list of sequence objects for the instance
        d:          cost limit
        """
        self.sequences = [Sequence(s.string, s.cost, s.pos, s.edit_sequence, s.lower_bound) for s in sequences]
        for i, s in enumerate(self.sequences):
            s.last_operation = sequences[i].last_operation
            s.previous_character = sequences[i].previous_character
            s.deleted_string = sequences[i].deleted_string
        self.solution = ''
        self.next_character = None
        self.good_path = True
        self.depth = 0
        self.character_priority = 0
        self.operation_priority = 0
        self.cost_limit = d
        self.next_operation = None
        self.next_mod_string_index = None

    def __lt__(self, other):
        return self.get_priority_tuple() < other.get_priority_tuple()

    def get_priority_tuple(self):
        """
        combines priority attributes into tuple to compare instance priority
        note values are negated because the priority queue prioritizes low values
        return tuple of priority attributes, ordered by their priority
        """
        return (
            - self.good_path,
            - self.depth,
            - self.character_priority,
            - self.operation_priority,
            self.alphabet.index(self.next_character) if self.next_character is not None else 0,
            ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
        )

    @abstractmethod
    def get_allowed_cost(self, sequence):
        """
        returns how many changes can still be made for a sequence
        sequence: sequence object for which to find remaining allowed changes
        return number of allowed changes for sequence (or instance, in case of Median String)
        """
        pass

    @abstractmethod
    def is_solved(self):
        """
        check whether instance is already solved and solution can be return
        return True if cost limit kept for all changes made and changes still to be made
        """
        pass

    @abstractmethod
    def is_unsolvable(self):
        """
        check if instance is in an unsolvable state
        return True if any indications for unsolvability found
        """
        pass

    def position_is_consistent(self):
        """
        check if all sequences have the same character in the currently examined position
        return True if all sequences have the same character in the current position
        """
        return len(set(map(lambda s: s.string[0], self.sequences))) == 1

    def get_possible_chars(self):
        """
        determine characters that can be put into the current position
        return list of possible characters
        """
        if self.next_character is not None:
            return self.next_character
        if self.options["quick_mode"]:
            return self.filter_characters([c for c in (set(map(lambda s: s.string[0] , self.sequences))) if c != '$'])
        return self.filter_characters(self.alphabet)

    @abstractmethod
    def filter_characters(self, chars):
        """
        filter potential characters for current position by instance properties
        chars: previously considered characters
        return list with subset of original characters that are viable
        """
        pass

    def find_mod_string_index(self, char):
        """
        determine first string to edit when putting certain character into current position
        char: next character
        return index of sequence which does not have selected character in current position
        """
        return next(i for i in range(len(self.sequences)) if self.sequences[i].string[0] != char)
        
    def max_num_deletions(self, sequence):
        """
        determines maximum number of deletions on a sequence to reach a plausible solution
        sequence:   sequence on which to determine maximum number of plausible deletions
        """
        return min(self.options['max_num_deletions'], self.cost_limit - sequence.cost)

    def get_possible_operations(self, char, mod_string_index):
        """
        determine possible operation to put character into a sequence
        char:               goal character for current position
        mod_string_index:   index of sequence to be edited
        return list of possible operations to put character into sequence
        """
        item = self.sequences[mod_string_index]
        if char == "$": 
            if char in item.string[0:self.get_allowed_cost(item)+1]:
                return ["del"]
            else:
                return []
        if item.string[0] == "$":
            return ["ins"]
        if item.last_operation == 'ins':
            return ["sub", "ins"]
        if char in item.string[0:self.max_num_deletions(item)+1]:
            if item.last_operation == 'del':
                return ["sub", "del"]
            else:
                return ["sub", "del", "ins"]
        if item.last_operation == 'del':
            return ["sub"]
        return ["sub", "ins"]

    def rank_character(self, mod_string_index, char):
        """
        calculate priority for a character at the current position
        mod_string_index:   index of sequence on which to evaluate character
        char:               character for which to calculate priority
        return priority for character (float between 0 and 1)
        """
        occurence = list(map(lambda s: s.string[0], self.sequences)).count(char)
        return occurence / len(self.sequences)
 
    def rank_operation(self, mod_string_index, operation, goal_char):
        """
        calculate priority for a operation and character at the current position
        operation:          operation to be considered
        mod_string_index:   index of sequence on which to evaluate operation
        goal_char:          character to be considered
        return priority for operation and character (float between 0 and 1)
        """ 
        item = self.sequences[mod_string_index].string
        goal_sequences = list(filter(lambda s: s.string[0] == goal_char, self.sequences))
        if len(goal_sequences) == 0:
            goal_sequences = self.sequences
        num_positions = min(self.options['lookahead_length'], max(map(lambda s: len(s.string), goal_sequences)))
        consensus_string = [list(map(lambda s: s.string[i] if i < len(s.string) else '-', goal_sequences)) for i in range(0, num_positions)]
        if operation == "sub":
            dist = functions.levenshtein_weighted(item[1:num_positions], consensus_string[1:])
        elif operation == "del":
            num_deletions = item.index(goal_char)
            dist = functions.levenshtein_weighted(item[num_deletions:num_deletions+num_positions], consensus_string)
        elif operation == "ins":
            dist = functions.levenshtein_weighted(item[:num_positions-1], consensus_string[1:])
        return (num_positions - dist) / num_positions

    def create_new_instance(self, mod_string_index, char, operation, char_priority, op_priority, good_path):
        """
        create child instance to evaluate
        mod_string_index:   index of sequence to edit
        char:               character to be put into current position
        operation:          edit operation to use to edit sequence
        char_priority:      priority of selected character
        operation_priority: priority of selected operation
        good_path:          whether the path instance is considered to be on a good path
        return new Instance object with edited sequence and updated cost limits
        """ 
        #create new instance object
        new_instance = type(self)(self.sequences, self.cost_limit)
        new_instance.alphabet = self.alphabet
        new_instance.solution = self.solution
        new_instance.options = self.options
        
        #set info on edit operation to perform
        new_instance.next_character = char
        new_instance.next_operation = operation
        new_instance.next_mod_string_index = mod_string_index
        
        #set priority values of new instance
        new_instance.good_path = good_path
        new_instance.depth = self.depth + 1
        new_instance.character_priority = char_priority
        new_instance.operation_priority = op_priority
        
        #debug information
        if self.options['debug']:
            print("priority for character " + char + " and operation " + operation + ": " + str(new_instance.operation_priority))
            if good_path:
                print("\t" + "selected as good path") 
            
        return new_instance

    def add_remaining_costs(self):
        """
        adds remaining costs for all sequences when instance is solved, but not all sequences are empty
        """ 
        for s in self.sequences:
            s.cost += len(s.string) - 1
            s.edit_sequence = s.edit_sequence + [[s.pos + i, "del", ""] for i in range(len(s.string) - 1)]
            
    def end_reached(self):
        return all(map(lambda s: s.string[0] == "$", self.sequences))

    def create_new_sequences(self):
        if all(map(lambda s: s.last_operation is None, self.sequences)):
            return [Sequence(s.string[1:], s.cost, s.pos + 1, s.edit_sequence, s.lower_bound) for s in self.sequences]
        for sequence in self.sequences:
            partner = self.sequences[sequence.lower_bound['partner']]
            lower_bound_change = -1
            #lower bound may change by more than one if more than one character deleted
            if sequence.last_operation == 'del':
                lower_bound_change = -len(sequence.deleted_string)
            #if same character inserted into both sequences, distance doesnt change
            if sequence.last_operation == partner.last_operation and sequence.last_operation == 'ins':
                lower_bound_change = 0
            #if same character substituted in both sequences, distance doesnt change
            elif sequence.last_operation == 'sub' and sequence.last_operation == partner.last_operation and sequence.previous_character == partner.previous_character:
                lower_bound_change = 0
            #if same characters deleted in both sequences, distance doesnt change
            elif sequence.last_operation == 'del' and sequence.last_operation == partner.last_operation and sequence.deleted_string == partner.deleted_string:
                lower_bound_change = 0
            sequence.lower_bound['value'] = max(sequence.lower_bound['value'] + lower_bound_change, 0)
        return [Sequence(s.string[1:], s.cost, s.pos + 1, s.edit_sequence, s.lower_bound) for s in self.sequences]

    def evaluate(self):
        """
        evaluate current instance for solution or child instances
        return dictionary with following values:
            solved: True if a solution has been found in the current instance
            solution: if solved, solution is the current instance
            children: if current instance is not solved, list with children instances to evaluate
        """ 

        #debug information
        if self.options['debug']:
            print("evaluating instance with following priority:")
            print(self.get_priority_tuple())
            print("character for current position: " + str(self.next_character))
            
        #apply edit operation
        if self.next_operation is not None:
            #edit sequence with goal character and operation
            item = self.sequences[self.next_mod_string_index]
            (modified_string, cost) = functions.modify_string(item.string, self.next_character, self.next_operation)
            deleted_string = None
            if self.next_operation == "sub":
                new_pos = item.pos
                new_sequence = item.edit_sequence + [[item.pos, "sub", self.next_character]]
            elif self.next_operation == "del":
                new_pos = item.pos + cost
                new_sequence = item.edit_sequence + [[item.pos + i, "del", ""] for i in range(cost)]
                deleted_string = item.string[:item.string.index(self.next_character)]
            elif self.next_operation == "ins":
                new_pos = item.pos - 1
                new_sequence = item.edit_sequence + [[item.pos, "ins", self.next_character]]
            new_sequence = Sequence(modified_string, item.cost + cost, new_pos, new_sequence, item.lower_bound)
            new_sequence.last_operation = self.next_operation
            new_sequence.previous_character = item.string[0]
            new_sequence.deleted_string = deleted_string
            self.sequences[self.next_mod_string_index] = new_sequence
            self.next_operation = None
     
        #skip positions where all sequences have the same character
        positions_skipped = 0
        while self.position_is_consistent() and not self.end_reached():
            self.solution = self.solution + self.sequences[0].string[0]
            self.next_character = None
            self.sequences = self.create_new_sequences()
            positions_skipped += 1
             
        #debug information
        if self.options['debug']:
            print(str(positions_skipped) + " positions skipped.")
            print("info on sequence status")
            for s in self.sequences:
                print(s)
                
        #check if instance looks unsolvable
        if self.is_unsolvable():
            if self.options['debug']:
                print("instance seems unsolvable.")
            return {'solved': False, 'children': []}
            
        #return solution if instance is solved
        if self.is_solved():
            self.add_remaining_costs()
            return {'solved': True, 'solution': self}
            
        #instance might be solvable, but is not solved yet - fetch child instances
        children = []
            
        #get possible characters for current position and get their priority
        chars = self.get_possible_chars()
        
        num_good_paths = 0
        for char in chars:
            #find string which to modify
            mod_string_index = self.find_mod_string_index(char)
            char_priority = self.rank_character(mod_string_index, char)
            operations = self.get_possible_operations(char, mod_string_index)
            new_instances_by_operation = {}
            best_operation_priority = -1
            best_operation = False
            if len(operations) > 0:
                operation_priorities = {op: self.rank_operation(mod_string_index, op, char) for op in operations}
                good_operations = [op for op in operations if operation_priorities[op] == max(operation_priorities.values())]
                for operation in operations:
                    is_good_path = char_priority > 0 and operation in good_operations
                    #if we are near the end and words have different lengths, things can get confusing
                    if min(map(lambda s: len(s.string), self.sequences)) <= 2:
                        is_good_path = True
                    new_instance = self.create_new_instance(mod_string_index, char, operation, char_priority, operation_priorities[operation], is_good_path)
                    children.append(new_instance)
        if self.options['debug']:
            print(str(len(children)) + " new instances created.")
            pdb.set_trace()
        return {'solved': False, 'children': children}
