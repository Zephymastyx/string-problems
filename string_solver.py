import time
import argparse
import os
import csv
import json
from queue import PriorityQueue
import functions
from centre_string import CentreStringInstance
from median_string import MedianStringInstance
from string_problem import Sequence

def get_distance_info(file):
    """
    does something nice.
    """
    distance_file = file[:-4] + "_dist.csv"
    if not os.path.isfile(distance_file):
        return functions.calc_distance_values(functions.read_sequences(file), False, False)
    res = {}
    with open(distance_file) as cf:
        reader = csv.reader(cf, delimiter=':')
        for row in reader:
            if row[0] == "max_median_d" or row[0] == "min_median_d" or row[0] == "max_centre_d" or row[0] == "min_centre_d":
                res[row[0]] = json.loads(row[1])
    return res

def write_solution(filename, solved_instance, verbose, runtime):
    """
    writes solution string and info on distances into file
    filename:           file in which to write solution
    solved_instance:    solved instance that contains solution
    verbose:            whether to print solution details
    runtime:            Run time to find solution in seconds
    """
    res = open(filename, 'w')
    res.write("solution;" + solved_instance.solution + "\n")
    max_dist = max(solved_instance.sequences, key=lambda s: s.cost).cost
    sum_dist = sum(map(lambda s: s.cost, solved_instance.sequences))
    avg_dist = round(sum_dist / len(solved_instance.sequences), 1)
    res.write("time;" + str(runtime) + "\n")
    res.write("max_dist;" + str(max_dist) + "\n")
    res.write("sum_dist;" + str(sum_dist) + "\n")
    res.write("avg_dist;" + str(avg_dist) + "\n")
    res.write("distances")
    for s in solved_instance.sequences:
        res.write(";" + str(s.cost))
    res.write("\n")
    res.write("edit sequences" + "\n")
    for s in solved_instance.sequences:
        res.write(str(s.edit_sequence) + "\n")
    res.close()
    if verbose:
        print("solution string:")
        print(solved_instance.solution[:30] + ("..." if len(solved_instance.solution) > 30 else ''))
        print("max distance to an input string: " + str(max_dist))
        print("sum of distances: " + str(sum_dist))
        print("average distance to input strings: " + str(avg_dist))
        print("distances to each sequence:")
        print(", ".join(list(str(s.cost) for s in solved_instance.sequences)))
        
def write_failure(filename, d, runtime):
    """
    writes info about unsolved instance
    filename:           file in which to write info
    d:                  highest d for which a solution was searched for
    runtime:            Run time until failure or abort in seconds
    """
    res = open(filename, 'w')
    res.write("no_solution;" + "\n")
    res.write("highest_d;" + str(d) + "\n")
    res.write("time;" + str(runtime) + "\n")
        
def find_solution(instance, verbose, max_num_instances = -1, good_paths_only = False):
    instances = PriorityQueue()
    instances.put(instance)
    
    #determine running time and number of evaluated instances
    start_time = time.time()
    num_instances = 0
    
    #work with instances from priority queue until solution is found or all instances have been evaluated
    solved = False
    while not solved and not instances.empty() and (max_num_instances == -1 or num_instances < max_num_instances):
        res = instances.get().evaluate()
        num_instances += 1
        solved = res['solved']
        if solved:
            solved_instance = res['solution']
        else:
            for child in res['children']:
                if not good_paths_only or child.good_path:
                    instances.put(child)
            if instances.qsize() > 250000:
                if verbose:
                    print("Too many instances in queue. Aborted.")
                break
    res = {
        'solved': solved,
        'num_instances': num_instances, 
        'time': time.time() - start_time,
    }
    if solved:
        res['solved_instance'] = solved_instance
    return res

if __name__ == "__main__":
    #parse arguments
    parser = argparse.ArgumentParser(description="Calculates Centre or Median String for given set of sequences.")
    parser.add_argument('filename', help='path to file with sequences')
    parser.add_argument('-d', "--distance", type=int, help='distance limit')
    parser.add_argument("-t", "--type", choices=['centre', 'median'], default='centre', help="type of problem")
    parser.add_argument("-p", "--parse_alphabet", help="parse input alphabet instead of using DNA alphabet", action="store_true")
    parser.add_argument("-db", "--debug", help="show debug info while branching", action="store_true")
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument("-q", "--quick", help="only evaluate paths with high probability to find solution", action="store_true")
    parser.add_argument('-b', "--binary_search", help='determine solution for unknown d', action="store_true")
    parser.add_argument("-l", "--limit", help="limit how many subproblems to evaluate per instance (0: no limit, n: (n * max_depth) instances). Default 1", type=int, default=1)
    args = parser.parse_args()
    
    if not args.binary_search and args.distance is None:
        parser.error("Distance limit or binary search option need to be set.")
        
    #read the sequences from input file
    sequences = functions.read_sequences(args.filename)
    
    #fetch the alphabet over which to solve the instance
    if args.parse_alphabet:
        alphabet = functions.get_alphabet(sequences)
    else:
        alphabet = ['A','C','G','T']
        
    #check if sequences match the specified alphabet
    if not functions.validate_alphabet(sequences, alphabet):
        parser.error("Alphabet does not match sequences!")
        
    #initialize problem instance
    d = args.distance
    sequences = list(map(lambda x: x + "$", sequences))
    
    #calculate distance values to determine bounds for cost limit
    if args.verbose:
        print("precalculating distances.")
    d_info = get_distance_info(args.filename)
    if args.binary_search:
        max_d = d_info["max_" + args.type + "_d"]
        min_d = d_info["min_" + args.type + "_d"]
        high = max_d
        low = min_d
    else:
        high = args.distance
        low = args.distance
        
    #perform binary search for optimal cost limit
    best_d = None
    best_solution = None
    while high >= low:
        cur_d = (high + low) // 2
        sequence_objects = [Sequence(
                s, 0, 0, [],
                {
                    'value':0,#max(list(d_info["distances"][i].values()) + [0]) - cur_d, 
                    'partner': 0#max(d_info["distances"][i], key=lambda k: d_info["distances"][i][k])
                }
            ) for i, s in enumerate(sequences)]
        if args.type == 'centre':
            instance = CentreStringInstance(sequence_objects, cur_d)
        else:
            instance = MedianStringInstance(sequence_objects, cur_d)
        options = {
            'debug': args.debug,
            'quick_mode': args.quick,
            'max_num_deletions': 5,
            'lookahead_length': 10,
        }
        instance.alphabet = alphabet
        instance.options = options
        
        max_depth = cur_d if args.type == "median" else cur_d * len(sequences)
        max_num_instances = -1
        if args.limit > 0:
            max_num_instances = max_depth * args.limit
        
        if args.verbose:
            print("evaluating instances for d = " + str(cur_d) + ".")
        res = find_solution(instance, args.verbose, max_num_instances, args.quick)
            
        #result output
        if args.verbose:
            print(str(res["time"]) + " seconds")
            print(str(res["num_instances"]) + " instances evaluated")
        if(res["solved"]):
            if args.verbose:
                print("found solution.")
            best_solution = res["solved_instance"]
            found_d = cur_d
            if args.type == "centre":
                found_d = max(res["solved_instance"].sequences, key=lambda s: s.cost).cost
            elif args.type == "median":
                found_d = sum(map(lambda s: s.cost, res["solved_instance"].sequences))
            best_d = found_d
            high = found_d - 1
        else:
            if args.verbose or not args.binary_search:
                print("no solution found.")
            if not args.binary_search:
                write_failure(args.filename[:-4] + "_" + args.type + "_string.csv", cur_d, res["time"])
            low = cur_d + 1
    filename_solution = args.filename[:-4] + "_" + args.type + "_string.csv"
    if best_solution is not None:
            write_solution(filename_solution, best_solution, args.verbose, res["time"])
            print("solution written into " + filename_solution)
    if args.binary_search:
        if args.verbose:
            print("binary search for optimal d complete.")
        if best_d is not None:
            if args.verbose:
                print("best d found: " + str(best_d) + ".")
                print("boundary for d: " + str(min_d) + "-" + str(max_d) + ".")
        else:
            print("no solution found for any d.")
            write_failure(filename_solution, cur_d, res["time"])
