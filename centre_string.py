import functions
from string_problem import Sequence, StringProblemInstance

class CentreStringInstance(StringProblemInstance):

    def get_allowed_cost(self, sequence):
        return self.cost_limit - sequence.cost

    def is_solved(self):
        return all(map(lambda s: len(s.string) <= self.cost_limit - s.cost + 1, self.sequences))

    def is_unsolvable(self):
        #instance can't be unsolvable if it's already solved
        if self.is_solved():
            return False
        #if instance is not yet solved and no sequence can be still changes, we can give up
        if all(map(lambda s: (s.cost + s.lower_bound['value']) >= self.cost_limit, self.sequences)):
            return True
        #if any lower bound of changes esceeds the remaining allowed changes, we can give up.
        #Note since lower bounds are only updated after the position has been resolved, this can only be done afterwards.
        if self.next_character is None and any(map(lambda s: (s.cost + s.lower_bound['value']) > self.cost_limit, self.sequences)):
            return True

        #no criteria fullfilled for unsovability - probably solvable
        return False

    def filter_characters(self, chars):
        res = []
        #filter out characters where a sequence does not have goal character and no changes are allowed
        for char in chars:
            if len(list(filter(lambda s: s.string[0] != char and s.cost == self.cost_limit, self.sequences))) == 0:
                res.append(char)
        return res

    def rank_character(self, mod_string_index, char):
        occurence = list(map(lambda s: s.string[0], self.sequences)).count(char)
        occurence_rank = occurence / len(self.sequences)

        change_sequences = list(filter(lambda s: s.string[0] != char, self.sequences))
        sum_costs = sum(map(lambda s: s.cost, change_sequences))
        remaining_cost_rank = 1 - (sum_costs / (len(change_sequences) * self.cost_limit))
        
        return occurence_rank * remaining_cost_rank
