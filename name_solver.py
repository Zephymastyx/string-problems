import argparse
from queue import PriorityQueue
import functions
from name_problem import CentreNameInstance, MedianNameInstance
from string_problem import Sequence
import unicodedata
import string

def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')

def find_solution(instance, max_num_instances = -1):
    instances = PriorityQueue()
    instances.put(instance)
    num_instances = 0
    solutions = []
    while not instances.empty() and (max_num_instances == -1 or num_instances < max_num_instances):
        res = instances.get().evaluate()
        num_instances += 1
        solved = res['solved']
        if solved:
            solutions.append(res['solution'].solution)
        else:
            for child in res['children']:
                instances.put(child)
    res = {
        'solved': len(solutions) > 0,
        'solutions': solutions
    }
    return res

if __name__ == "__main__":
    #parse arguments
    parser = argparse.ArgumentParser(description="Calculates Centre or Median String for given set of sequences.")
    parser.add_argument("-f", "--filename", help='file with names, one name per line')
    parser.add_argument("-n", "--names", help='list of names, comma seperated')
    parser.add_argument("-g", "--gender", choices=['a', 'f', 'm'], default='any', help="gender of the name - *a*ny, *f*emale, or *m*ale")
    parser.add_argument("-w", "--wait", choices=['s', 'm', 'l', 'f'], default='s', help="how long you are willing to wait for solutions - *s*hort, *m*edium, *l*ong, or *f*orever")
    args = parser.parse_args()
    
    if args.filename is None and args.names is None:
        parser.error("List of names or filename of name list needs to be specified!")
    if args.filename is not None and args.names is not None:
        parser.error("Please either specify a list of names, or a file. Not both.")
        
    #read the sequences from input file
    if args.filename is not None:
        sequences = functions.read_sequences(args.filename)
    if args.names is not None:
        sequences = [name for name in args.names.split(",")]
    sequences = [sequence.strip().lower() for sequence in sequences]
    sequences_without_accents = [strip_accents(sequence) for sequence in sequences]
    if sequences != sequences_without_accents:
        print("Note: Special characters from names have been removed.")
        sequences = sequences_without_accents
        
    
    #fetch the alphabet over which to solve the instance
    alphabet = string.ascii_lowercase
    if not functions.validate_alphabet(sequences, alphabet):
        parser.error("Some of the characters are not in the alphabet. \nPlease don't do that.")
    sequences = list(map(lambda x: x + "$", sequences))
    
    type = "centre"
    
    if args.wait == "s":
        limit = 20
    elif args.wait == "m":
        limit = 200
    elif args.wait == "l":
        limit = 5000
    elif args.wait == "f":
        print("WARNING: This will probably literally take forever.")
        limit = -1
    
    
    #calculate distance values to determine bounds for cost limit
    d_info = functions.calc_distance_values(sequences, False, False)
    max_d = d_info["max_" + type + "_d"]
    min_d = d_info["min_" + type + "_d"]
    high = max_d
    low = min_d
        
    #perform binary search for optimal cost limit
    best_solution = None
    while high >= low:
        cur_d = (high + low) // 2
        sequence_objects = [Sequence(
                s, 0, 0, [],
                {
                    'value':0,
                    'partner': 0
                }
            ) for i, s in enumerate(sequences)]
        if type == 'centre':
            instance = CentreNameInstance(sequence_objects, cur_d)
        else:
            instance = MedianNameInstance(sequence_objects, cur_d)
        options = {
            'debug': False,
            'quick_mode': False,
            'max_num_deletions': 5,
            'lookahead_length': 10,
            'gender': args.gender,
        }
        instance.alphabet = alphabet
        instance.options = options
        
        max_depth = cur_d if type == "median" else cur_d * len(sequences)
        max_num_instances = -1
        if limit > 0:
            max_num_instances = max_depth * limit

        res = find_solution(instance, max_num_instances)
            
        if(res["solved"]):
            best_solution = res["solutions"]
            high = cur_d - 1
        else:
            low = cur_d + 1
    if best_solution is not None:
        print("Some name suggestions:\n")
        for name in set(best_solution):
            print(name.capitalize())
    else:
        print("Sorry, could not find any good names :(")
