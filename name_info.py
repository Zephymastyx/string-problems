names = {}
valid_prefixes = {"m": {}, "f": {}, "a": {}}

for type in ["f", "m"]:
    with open("names_" + type + ".txt", 'r') as f:
        names[type] = [line.strip() for line in f]
        for i in range(1, len(max(names[type], key = lambda n: len(n)))):
            for name in [name for name in names[type] if len(name) >= i]:
                valid_prefixes[type][name[:i]] = True
                
names["a"] = list(set(names["f"] + names["m"]))
valid_prefixes["a"] = dict(list(valid_prefixes["f"].items()) + list(valid_prefixes["m"].items()))

def name_exists(prefix, type):
    """
    Checks if a name exists that stars with the given prefix
    prefix:  prefix to check
    return True iff a name exists starting with given prefix
    """
    return valid_prefixes[type].get(prefix, False)
 
def is_name(str, type):
    """
    Checks if given string is a name
    str:    string to check    
    return True iff given string is a name
    """
    return str in names[type]