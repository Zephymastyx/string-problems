import sys
sys.path.insert(0, '../')
import functions
import argparse
import itertools
import os
import glob
import math
from functools import reduce
from collections import OrderedDict

def calc_distance_values(sequences, remove_duplicates, verbose):
    #check for duplicates
    orig_num_sequences = len(sequences)
    sequence_set = set(sequences)
    num_duplicates = 0
    reduced_sequences = sequences
    if len(sequence_set) < len(sequences):
        num_duplicates = len(sequences) - len(sequence_set)
        if verbose:
            print(str(num_duplicates) + " duplicate sequences.")
        reduced_sequences = list(OrderedDict.fromkeys(sequences))
        if remove_duplicates:
            if verbose:
                print("duplicates removed.")
            sequences = reduced_sequences
            
    #initialize values for distance calculations
    indices = range(len(reduced_sequences))
    distances = [{} for i in range(len(sequences))]

    if verbose:
        print("calculating distances..")

    #calculate levenshtein distance for all not duplicated pairs of sequences
    for pair in itertools.combinations(indices, 2):
        s = reduced_sequences[pair[0]]
        t = reduced_sequences[pair[1]]
        d = functions.levenshtein(s, t)
        
        #save distances for all copies of sequence
        indices_s = [i for i, x in enumerate(sequences) if x == s]
        indices_t = [i for i, x in enumerate(sequences) if x == t]
        for index_s in indices_s:
            for index_t in indices_t:
                distances[index_s][index_t] = d
                distances[index_t][index_s] = d
                
    #add null values to distance dicts
    for d in distances:
        for i in range(len(distances)):
            if i not in d:
                d[i] = 0

    #determine bounds for centre string instances
    max_centre_d = min(map(lambda x: max(x.values()), distances))
    min_centre_d = math.ceil(max(map(lambda x: max(x.values()), distances)) / 2)
    
    #determine bounds for median string instances
    max_median_d = min(map(lambda x: sum(x.values()), distances))
    arbitary_path_cost = sum(map(lambda i: distances[i][(i+1) % len(distances)], range(len(distances))))
    min_median_d = arbitary_path_cost // 2 
    
    #determine average distance of insider and outsider sequence to other sequences
    avg_min = round(max_median_d / (len(sequences) - 1), 1)
    avg_max = round(max(map(lambda x: sum(x.values()), distances)) / (len(sequences) - 1), 1)
    
    #determine average length of sequences and error rate
    sum_dist = sum(map(lambda x: sum(x.values()), distances)) / 2
    num_pairs = int((len(sequences) * (len(sequences)-1)) / 2)
    avg_dist = int(round(sum_dist / num_pairs))
    avg_length = int(round(reduce((lambda x, s: len(s) + x), sequences, 0) / len(sequences)))
    error_rate = round(avg_length / avg_dist, 1)
    return {
        "distances" : distances,
        "orig_num_sequences" : orig_num_sequences,
        "duplicates" : num_duplicates,
        "avg_len" : avg_length,
        "max_centre_d" : max_centre_d,
        "min_centre_d" : min_centre_d,
        "max_median_d" : max_median_d,
        "min_median_d" : min_median_d,
        "avg_dist_min" : avg_min,
        "avg_dist_max" : avg_max,
        "error_rate" : error_rate,
    }

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="calculates levenshtein distance for all pairs of sequences in file and writes maximum and average distance into <filename>_dist.csv")
    parser.add_argument('path', help='path to folder with files to evaluate')
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    parser.add_argument("-r", "--remove_duplicates", help="remove duplicated sequences before calculating distances", action="store_true")
    args = parser.parse_args()
    os.chdir(args.path)
    for f in glob.glob("*.fas"):
        if args.verbose:
            print("reading file " + f)
            
        #get list of sequences
        sequences = functions.read_sequences(f)
        
        res = calc_distance_values(sequences, args.remove_duplicates, args.verbose)
        res_file = open(f[:-4] + "_dist.csv", 'w')
        for key in res:
            res_file.write(key + ":" + str(res[key]) + "\n")
        res_file.close()
        if args.verbose:
            print("done.")
        
    if args.verbose:
        print("all files read.")
        