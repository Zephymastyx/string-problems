import sys
sys.path.insert(0, '../')
import argparse
import functions
import random
import itertools
import time
import csv
import json

def generate_sequences(alphabet, num_sequences, length, num_changes, operation_probabilities):
    """
    generates a number of random sequences with a set number of changes
    alphabet:           the alphabet over which to create the sequence
    num_sequences:      number of sequences to create
    length:             base length of each sequence
    fraction_changes:   number of positions to perform changes on (not greater than sequence length)
    op_probabilities:   dict of probabilities for each edit operation
    return dict with base sequence and list of changed sequences
    """
    #create base sequence from which parts are changed
    base_sequence = ''
    for i in range(length):
        char = random.choice(alphabet)
        base_sequence += char
        
    #create list of sequences
    sequences = [base_sequence for i in range(num_sequences)]
    all_positions = [i for i in range(length)]
    changed_positions = random.sample(all_positions, num_changes)
    
    #iterate over random selection of positions where changes occur
    for pos in changed_positions:
        #determine selection of sequences to change
        num_changed_sequences = random.randint(1, num_sequences // 2)
        changed_sequences_indices = random.sample([j for j in range(num_sequences)], num_changed_sequences)
        
        #get list of possible characters to change to
        considered_alphabet = list(alphabet)
        considered_alphabet.remove(base_sequence[pos])
        
        #iterate over sequences to change
        for i in changed_sequences_indices:
            #select new character for position
            new_char = random.choice(considered_alphabet)
            
            #determine edit operation to use
            r = random.random()
            sum_items = 0
            for op in operation_probabilities:
                if r < sum_items + operation_probabilities[op]:
                    chosen_operation = op
                    break
                sum_items += operation_probabilities[op]
                
            #use edit operation on sequence
            sequence = sequences[i]
            if chosen_operation == 'sub':
                sequence = sequence[:pos] + new_char + sequence[pos+1:]
            elif chosen_operation == 'del':
                sequence = sequence[:pos] + sequence[pos+1:]
            elif chosen_operation == 'ins':
                sequence = sequence[:pos] + new_char + sequence[pos:]
            sequences[i] = sequence
    return {
        'base_sequence': base_sequence, 
        'sequences' : sequences
    }
    
def validate_options(options):
    if ["alphabet", "lengths", "num_sequences", "num_changes"] - options.keys():
        return False
    if not "fraction_substitutions" in options.keys() and not "operation_probabilities" in options.keys():
        return False
    if max(options["num_changes"]) > min(options["lengths"]):
        return False
    if "operation_probabilities" in options.keys():
        operation_probabilities = options["operation_probabilities"]
    else:
        operation_probabilities = [{
            "sub": val,
            "ins": (1 - val) / 2,
            "del": (1 - val) / 2,
        } for val in options["fraction_substitutions"]]
    if not all(map(lambda p: abs(sum(p.values()) - 1) < 0.001, operation_probabilities)):
        return False
    return True

if __name__ == "__main__":
    #parse arguments
    parser = argparse.ArgumentParser(description="Generates a number of sets of random sequences.")
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    args = parser.parse_args()
    
    #get setting options from config file
    options = {}
    with open('config.csv', 'r') as config:
        reader = csv.reader(config, delimiter=";")
        for row in reader:
            options[row[0]] = json.loads(row[1])
    if not validate_options(options):
        parser.error("config file could not be validated")
        
    file_suffix = ""
    if "file_suffix" in options:
        file_suffix = "_" + options["file_suffix"][0]
        
    num_files = 1
    if "num_files" in options:
        num_files = options["num_files"]
        
    #calculate operation chances if only substitution fraction given
    if "fraction_substitutions" in options.keys():
        options["operation_probabilities"] = [{
            "sub": val,
            "ins": (1 - val) / 2,
            "del": (1 - val) / 2,
        } for val in options["fraction_substitutions"]]

    #get random seed
    random.seed(42)
    
    num_combination = 1
    total_combinations = len(list(itertools.product(options["lengths"], options["num_sequences"], options["num_changes"], options["operation_probabilities"])))
    #iterate over all setting combinations
    for combination in itertools.product(options["lengths"], options["num_sequences"], options["num_changes"], options["operation_probabilities"]):
        if args.verbose:
            print("using setting combination " + str(num_combination) + "/" + str(total_combinations) + ".")
        cur_length = combination[0]
        cur_num_sequences = combination[1]
        cur_num_changes = combination[2]
        cur_operation_probabilities = combination[3]
        
        for i in range(num_files):
            if args.verbose:
                print("creating file " + str((num_combination - 1) * num_files + i + 1) + "/" + str(total_combinations * num_files) + ".")
            #generate sequences
            res = generate_sequences(options["alphabet"], cur_num_sequences, cur_length, cur_num_changes, cur_operation_probabilities)
            
            #suffix based on setting and file number
            full_file_suffix = file_suffix + "_" + str(num_combination) + "_" + str(i+1)
            
            #create file with sequences
            res_file = open("generated_sequences/random_sequences" + full_file_suffix + ".fas", 'w')
            for sequence in enumerate(res['sequences']):
                res_file.write(">" + time.strftime("%Y%m%d") + "|random" + full_file_suffix + "_" + str(sequence[0] + 1) + "\n")
                res_file.write(sequence[1] + "\n")
            res_file.close()
            
            #calculate distances to preserve (theoretical) optimal d for a solution
            distances = list(map(lambda s: functions.levenshtein(s, res['base_sequence']), res['sequences']))
            centre_d = max(distances)
            median_d = sum(distances)
                
            #write base sequence and optimal values for d into second file
            solution_file = open("generated_sequences/random_sequences" + full_file_suffix + "_solution.txt", 'w')
            solution_file.write(res['base_sequence'] + "\n")
            solution_file.write("optimal centre string d:" + str(centre_d) + "\n")
            solution_file.write("optimal median string d:" + str(median_d) + "\n")
            solution_file.write("settings:" + str(combination) + "\n")
            solution_file.close()
        num_combination += 1
