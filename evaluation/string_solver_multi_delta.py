import sys
sys.path.insert(0, '../')
import time
import argparse
import os
import glob
import math
from queue import PriorityQueue
import functions
from centre_string import CentreStringInstance
from median_string import MedianStringInstance
from string_problem import Sequence
from string_solver import write_solution, find_solution, write_failure
from calc_distances import calc_distance_values
import csv

def asdf(f):
    fd = f[:-4] + "_dist.csv"
    if not os.path.isfile(fd):
        return calc_distance_values(functions.read_sequences(f), False, False)
    res = {}
    with open(fd) as cf:
        reader = csv.reader(cf, delimiter=':')
        for row in reader:
            if row[0] == "max_median_d" or row[0] == "min_median_d" or row[0] == "max_centre_d" or row[0] == "min_centre_d":
                res[row[0]] = int(row[1])
    return res

if __name__ == "__main__":
    #parse arguments
    parser = argparse.ArgumentParser(description="Calculates Centre or Median String for all files in a folder.")
    parser.add_argument('path', help='path to folder with sequence files')
    parser.add_argument("-t", "--type", choices=['centre', 'median'], default='centre', help="type of problem")
    parser.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    args = parser.parse_args()
    
    args = parser.parse_args()
    os.chdir(args.path)
    num_total = 0
    num_solved = 0
    start_time_global = time.time()
    num_files = len(glob.glob("*.fas"))
    
    d_infos = {}
    if args.verbose:
        print("precalculating distances...")
    for f in glob.glob("*.fas"):
        if "random" in f and "eval_delta" not in f:
            continue
        d_infos[f] = asdf(f)
    if args.verbose:
        print("distances calculated.")

    for delta in [1, 2, 3, 4, 5, 10]:
        i = 1
        num_total = 0
        num_solved = 0
        start_time_global = time.time()
        if args.verbose:
            print("delta = " + str(delta))
        for f in glob.glob("*.fas"):
            if "random" in f and "eval_delta" not in f:
                continue
            if args.verbose:
                print("reading file " + str(i) + "/" + str(num_files) + ": " + f)
                i += 1
            
            #read the sequences from input file
            sequences = functions.read_sequences(f)
            alphabet = ['A','C','G','T']
                
            #check if sequences match the specified alphabet
            if not functions.validate_alphabet(sequences, alphabet):
                print(f + ": Alphabet does not match sequences!")
                continue
                
            #calculate distance values to determine bounds for cost limit
            if args.verbose:
                print("precalculating distances.")
            d_info = d_infos[f]
            max_d = d_info["max_" + args.type + "_d"]
            min_d = d_info["min_" + args.type + "_d"]
            high = max_d
            low = min_d
            sequences = list(map(lambda x: x + "$", sequences))
            
            #perform binary search for optimal cost limit
            best_d = None
            while high >= low:
                cur_d = (high + low) // 2
                #initialize problem instance
                max_depth = cur_d if args.type == "median" else cur_d * len(sequences)
                sequence_objects = [Sequence(
                    s, 0, 0, [],
                    {
                        'value': 0, 
                        'partner': 0
                    }
                ) for i, s in enumerate(sequences)]
                if args.type == 'centre':
                    instance = CentreStringInstance(sequence_objects, cur_d)
                else:
                    instance = MedianStringInstance(sequence_objects, cur_d)
                options = {
                    'debug': False,
                    'quick_mode': True,
                    'max_num_deletions': 5,
                    'lookahead_length': 10,
                }
                instance.alphabet = alphabet
                instance.options = options
                max_num_instances = max_depth * delta
                    
                if args.verbose:
                    print("evaluating instances for d = " + str(cur_d) + ".")
                res = find_solution(instance, args.verbose, max_num_instances, True)
                    
                #result output
                if args.verbose:
                    print(str(res["time"]) + " seconds")
                    print(str(res["num_instances"]) + " instances evaluated")
                if(res["solved"]):
                    filename_solution = f[:-4] + "_" + args.type + "_delta" + str(delta) + "_string.csv"
                    write_solution(filename_solution, res["solved_instance"], False, res["time"])
                    found_d = cur_d
                    if args.type == "centre":
                        found_d = max(res["solved_instance"].sequences, key=lambda s: s.cost).cost
                    elif args.type == "median":
                        found_d = sum(map(lambda s: s.cost, res["solved_instance"].sequences))
                    if args.verbose:
                        print("found solution with d = " + str(found_d) + ".")
                    best_d = found_d
                    high = found_d - 1
                else:
                    if args.verbose and max_num_instances != -1 and res["num_instances"] >= max_num_instances:
                        print("maximum amount of instances evaluated.")
                    if args.verbose:
                        print("no solution found.")
                    low = cur_d + 1
            if args.verbose:
                print("binary search for optimal d complete.")
            if best_d is not None:
                num_solved += 1
                if args.verbose:
                    print("best d found: " + str(best_d) + ".")
            else:
                if args.verbose:
                    print("no solution found for any d.")
                filename_solution = f[:-4] + "_" + args.type + "_delta" + str(delta) + "_string.csv"
                write_failure(filename_solution, cur_d, res["time"])
            num_total += 1
        if args.verbose:
            print("all files read.")
            print("total time: " + str(time.time() - start_time_global))
            print(str(num_solved) + "/" + str(num_total) + " instances solved.")
