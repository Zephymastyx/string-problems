import sys
sys.path.insert(0, '../')
import functions
from centre_string import CentreStringInstance
from median_string import MedianStringInstance
from string_problem import Sequence, StringProblemInstance

class CentreStringPrioInstance(CentreStringInstance):

    def get_priority_tuple(self):
        if self.options["prio_function"] == 1:
            return (
                - self.good_path,
                - self.depth,
                - self.character_priority,
                - self.operation_priority,
                self.alphabet.index(self.next_character) if self.next_character is not None else 0,
                ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
            )
        if self.options["prio_function"] == 2:
            return (
                - self.depth,
                - self.character_priority,
                - self.operation_priority,
                self.alphabet.index(self.next_character) if self.next_character is not None else 0,
                ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
            )
        if self.options["prio_function"] == 3:
            return (
                - self.depth,
                - self.character_priority,
                self.alphabet.index(self.next_character) if self.next_character is not None else 0,
                ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
            )
        if self.options["prio_function"] == 4:
            return (
                - self.depth,
                self.alphabet.index(self.next_character) if self.next_character is not None else 0,
                ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
            )
        return 0
        
class MedianStringPrioInstance(MedianStringInstance):

    def get_priority_tuple(self):
        if self.options["prio_function"] == 1:
            return (
                - self.good_path,
                - self.depth,
                - self.character_priority,
                - self.operation_priority,
                self.alphabet.index(self.next_character) if self.next_character is not None else 0,
                ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
            )
        if self.options["prio_function"] == 2:
            return (
                - self.depth,
                - self.character_priority,
                - self.operation_priority,
                self.alphabet.index(self.next_character) if self.next_character is not None else 0,
                ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
            )
        if self.options["prio_function"] == 3:
            return (
                - self.depth,
                - self.character_priority,
                self.alphabet.index(self.next_character) if self.next_character is not None else 0,
                ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
            )
        if self.options["prio_function"] == 4:
            return (
                - self.depth,
                self.alphabet.index(self.next_character) if self.next_character is not None else 0,
                ["sub","del","ins"].index(self.next_operation) if self.next_operation is not None else 0,
            )
        return 0