﻿import argparse
import os
import glob
import csv
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='path to folder with files to evaluate')
    args = parser.parse_args()
    os.chdir(args.path)
    
    def something(type):
        prios = [1, 2, 3, 4]
        num_solved = {}
        num = 0
        for p in prios:
            num_solved[p] = 0
        for f in glob.glob("*.fas"):
            num += 1
            fd = f[:-4] + "_dist.csv"
            with open(fd) as cf:
                reader = csv.reader(cf, delimiter=':')
                for row in reader:
                    if row[0] == "max_" + type + "_d":
                        upper_bound = int(row[1])
                if row[0] == "min_" + type + "_d":
                    lower_bound = int(row[1])
            for p in prios:
                fa = f[:-4] + "_" + type + "_string_p" + str(p) + ".csv"

                with open(fa) as cf:
                    reader = csv.reader(cf, delimiter=';')
                    solved = False
                    for row in reader:
                        if row[0] == "solution":
                            num_solved[p] += 1
                            solved = True
        
        yn = []
        for p in prios:
            yn.append(round(num_solved[p] / num, 2))
        N = 4
        ind = np.arange(N)
        width = 0.7

        fig, ax = plt.subplots()
        rects1 = ax.bar(ind+0.15, yn, width, color='#00dd00')
        ax.set_ylabel('Anteil gelöster Instanzen')
        ax.set_ylim([0, 1])
        
        ax.set_xticks(ind+(width/2)+0.15)
        ax.set_xticklabels( ('P1', 'P2', 'P3', 'P4') )
        ax.set_xlabel(type + " string")
        
        def autolabel(rects):
            # attach some text labels
            for rect in rects:
                height = rect.get_height()
                ax.text(rect.get_x()+rect.get_width()/2., height - 0.04 if height > 0 else 0, float(height),
                        ha='center', va='bottom')

        autolabel(rects1)    
        plt.show()
        
    something("centre")
    something("median")
    
    