﻿import argparse
import os
import glob
import csv
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="calculates levenshtein distance for all pairs of sequences in file and writes maximum and average distance into <filename>_dist.csv")
    parser.add_argument('path', help='path to folder with files to evaluate')
    args = parser.parse_args()
    os.chdir(args.path)
    
    lookaheads = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 30]
    num_solved = {}
    qual_solutions = {}
    runtime = {}
    num = 0
    for l in lookaheads:
        num_solved[l] = 0
        qual_solutions[l] = []
        runtime[l] = []
    
    for f in glob.glob("*.fas"):
        if "eval_l" not in f:
            continue
        num += 1
        fs = f[:-4] + "_solution.txt"
        with open(fs) as cf:
            reader = csv.reader(cf, delimiter=':')
            for row in reader:
                if row[0] == "optimal centre string d":
                    lower_bound = int(row[1])
        fd = f[:-4] + "_dist.csv"
        with open(fd) as cf:
            reader = csv.reader(cf, delimiter=':')
            for row in reader:
                if row[0] == "max_centre_d":
                    upper_bound = int(row[1])
        for lookahead in lookaheads:
            fa = f[:-4] + "_centre_l" + str(lookahead) + "_string.csv"

            with open(fa) as cf:
                reader = csv.reader(cf, delimiter=';')
                solved = False
                for row in reader:
                    if row[0] == "solution":
                        num_solved[lookahead] += 1
                        solved = True
                    if row[0] == "time":
                        if solved:
                            runtime[lookahead].append(float(row[1]))
                    if row[0] == "max_dist":
                        d = int(row[1])
                        q = (upper_bound - max(d, lower_bound)) / (upper_bound - lower_bound)
                        qual_solutions[lookahead].append(q)
    
    xt = []
    yn = []
    yt = []
    xq = []
    yq = []
    for l in lookaheads:
        yn.append(num_solved[l] / num)
        if(len(runtime[l]) > 0):
            yt.append(sum(runtime[l]) / len(runtime[l]))
            xt.append(l)
        if(len(qual_solutions[l]) > 0):
            yq.append(sum(qual_solutions[l]) / len(qual_solutions[l]))
            xq.append(l)
        
    
    fig, ax1 = plt.subplots()
    
    l1 = mlines.Line2D([], [], color='r', marker='o')
    l2 = mlines.Line2D([], [], color='g', marker='o')
    l3 = mlines.Line2D([], [], color='b', marker='o')
    fig.legend((l1, l2, l3), ('Laufzeit', 'Gelöste Instanzen', 'Lösungsqualität'), 'lower right', bbox_to_anchor=[0.9, 0.1])
    
    ax1.plot(xq, yq, color="b", marker='o', ls='dotted', clip_on=False)
    ax1.plot(lookaheads, yn, color="g", marker='o', ls='dotted', clip_on=False)
    ax1.set_xlabel(r'Lookahead $\ell$')
    ax1.set_ylabel('Durchschnittliche Lösungsqualität\nAnteil gelöster Instanzen')
    ax1.set_ylim([0,1])

    ax2 = ax1.twinx()
    ax2.plot(xt, yt, color="r", marker='o', ls='dotted', clip_on=False)
    ax2.set_ylabel('Durchschnittliche Laufzeit in s', color='r')
    for tl in ax2.get_yticklabels():
        tl.set_color('r')
    
    plt.show()