﻿import argparse
import os
import glob
import csv
import matplotlib.pyplot as plt
import numpy as np

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='path to folder with files to evaluate')
    args = parser.parse_args()
    os.chdir(args.path)
    
    asdf = []
    asdf2 = []
    num = 0
    stuff = []
    stuff2 = []
    qwert = []
    qwert2 = []
    
    for f in glob.glob("*.fas"):
        fd = f[:-4] + "_dist.csv"
        with open(fd) as cf:
            reader = csv.reader(cf, delimiter=':')
            for row in reader:
                if row[0] == "max_centre_d":
                    upper_bound = int(row[1])
                if row[0] == "min_centre_d":
                    lower_bound = int(row[1])
                if row[0] == "avg_len":
                    slen = int(row[1])
                if row[0] == "max_median_d":
                    upper_bound2 = int(row[1])
                if row[0] == "min_median_d":
                    lower_bound2 = int(row[1])
        stuff.append(upper_bound / lower_bound)
        stuff2.append(upper_bound2 / lower_bound2)
        num += 1
        fs = f[:-4] + "_centre_string.csv"

        with open(fs) as cf:
            reader = csv.reader(cf, delimiter=';')
            solved = False
            for row in reader:
                if row[0] == "solution":
                    solved = True
                if row[0] == "max_dist":
                    d = int(row[1])
                    q = (upper_bound - d) / max((upper_bound - lower_bound), 1)
                    if lower_bound == upper_bound:
                        q = 1
                    asdf.append(q)
                if row[0] == "time" and solved:
                    qwert.append(float(row[1]))

                    
        fs = f[:-4] + "_median_string.csv"

        
        with open(fs) as cf:
            reader = csv.reader(cf, delimiter=';')
            solved = False
            for row in reader:
                if row[0] == "solution":
                    solved = True
                if row[0] == "sum_dist":
                    d = int(row[1])
                    q = (upper_bound2 - d) / max((upper_bound2 - lower_bound2), 1)
                    if lower_bound == upper_bound:
                        q = 1
                    asdf2.append(q)
                    solved = True
                if row[0] == "time" and solved:
                    qwert2.append(float(row[1]))
        
    print("Auswertung Centre String Lösungen")
    print("Gelöste Instanzen: " + str(len(asdf)) + "/" + str(num) + " (" + str(round(100 * len(asdf) / num, 1)) + "%)")
    print("Durchschnittliche Lösungsqualität: " + str(round(sum(asdf) / len(asdf), 2)))
    print("Durchschnittliche Laufzeit: " + str(round(sum(qwert) / max(len(qwert), 1), 2)) + " Sekunden")
    print("Maximale Laufzeit: " + str(round(max(qwert), 2)) + " Sekunden")
    if(len(set(asdf)) > 1):
        print("Erzeuge Violin Plot über erzielte Lösungsqualitäten..")
        fig, ax = plt.subplots()
        ax.violinplot(asdf, showmeans = True)
        ax.set_ylabel('Lösungsqualität')
        plt.show()
    
    print()
    
    print("Auswertung Median String Lösungen")
    print("Gelöste Instanzen: " + str(len(asdf2)) + "/" + str(num) + " (" + str(round(100 * len(asdf2) / num, 1)) + "%)")
    print("Durchschnittliche Lösungsqualität: " + str(round(sum(asdf2) / len(asdf2), 2)))
    print("Durchschnittliche Laufzeit: " + str(round(sum(qwert2) / max(len(qwert2), 1), 2)) + " Sekunden")
    print("Maximale Laufzeit: " + str(round(max(qwert2), 2)) + " Sekunden")
    
    if(len(set(asdf2)) > 1):
        print("Erzeuge Violin Plot über erzielte Lösungsqualitäten..")
        fig, ax = plt.subplots()
        ax.violinplot(asdf2, showmeans = True)
        ax.set_ylabel('Lösungsqualität')
        plt.show()
    
    