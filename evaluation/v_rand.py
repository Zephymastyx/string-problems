﻿import argparse
import os
import glob
import csv
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np

def something(vals, type, xlabel, subplot, xticks = None):
        num_solved = {}
        qual_solutions = {}
        runtime = {}
        for i, val in enumerate(vals):
            num_solved[val] = 0
            qual_solutions[val] = []
            runtime[val] = []
            num = 0

            for f in glob.glob("*_" + type + "_" + str(i+1) + "*.fas"):
                num += 1
                fs = f[:-4] + "_solution.txt"
                with open(fs) as cf:
                    reader = csv.reader(cf, delimiter=':')
                    for row in reader:
                        if row[0] == "optimal " + problem + " string d":
                            lower_bound = int(row[1])
                fd = f[:-4] + "_dist.csv"
                with open(fd) as cf:
                    reader = csv.reader(cf, delimiter=':')
                    for row in reader:
                        if row[0] == "max_" + problem + "_d":
                            upper_bound = int(row[1])
                            
                fa = f[:-4] + "_" + problem + "_string.csv"
                with open(fa) as cf:
                    reader = csv.reader(cf, delimiter=';')
                    solved = False
                    for row in reader:
                        if row[0] == "solution":
                            num_solved[val] += 1
                            solved = True
                        if row[0] == "time":
                            if solved:
                                runtime[val].append(float(row[1]))
                        if (problem == "centre" and row[0] == "max_dist") or (problem == "median" and row[0] == "sum_dist"):
                            d = int(row[1])
                            q = (upper_bound - max(d, lower_bound)) / (max(upper_bound - lower_bound, 1))
                            if upper_bound == lower_bound:
                                q = 1
                            qual_solutions[val].append(q)
        
        xt = []
        yn = []
        yt = []
        xq = []
        yq = []

        for val in vals:
            yn.append(num_solved[val] / num)
            if(len(runtime[val]) > 0):
                yt.append(sum(runtime[val]) / len(runtime[val]))
                xt.append(val)
            if(len(qual_solutions[val]) > 0):
                yq.append(sum(qual_solutions[val]) / len(qual_solutions[val]))
                xq.append(val)
            
        
        ax1 = fig.add_subplot(subplot)
        
        ax1.plot(xq, yq, color="b", marker='o', ls='dotted', clip_on=False)
        ax1.plot(vals, yn, color="g", marker='o', ls='dotted', clip_on=False)
        ax1.set_xlabel(r"" + xlabel)
        if subplot % 2 == 1:
            ax1.set_ylabel('Lösungsqualität\nAnteil gelöster Instanzen')
        ax1.set_ylim([0,1])
        if xticks:
            ax1.set_xticks(xticks)
        ax1.set_xlim(left = 0)

        ax2 = ax1.twinx()
        ax2.plot(xt, yt, color="r", marker='o', ls='dotted', clip_on=False)
        if subplot % 2 == 0:
            ax2.set_ylabel('Durchschnittliche Laufzeit in s', color='r')
        for tl in ax2.get_yticklabels():
            tl.set_color('r')
        ax2.set_ylim(bottom = 0)
        ax2.set_xlim(left = 0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='path to folder with files to evaluate')
    args = parser.parse_args()
    os.chdir(args.path)
    
    
    plt.rcParams.update({'legend.fontsize': 8})
    l1 = mlines.Line2D([], [], color='r', marker='o')
    l2 = mlines.Line2D([], [], color='g', marker='o')
    l3 = mlines.Line2D([], [], color='b', marker='o')
    
    fig = plt.figure()
    problem = "centre"
    something([1000, 2000, 3000, 4000, 5000, 10000], "length", "Länge der Grundsequenz", 221, [1000, 3000, 5000, 10000])
    something([20, 50, 100, 200, 300, 400, 500, 1000], "d", "Anzahl veränderter Positionen", 222, [0, 200, 500, 1000]) 
    something([5, 10, 20, 30, 40, 50], "n", "Anzahl der Sequenzen", 223, [5, 10, 20, 30, 40, 50])
    something([0, 0.2, 0.4, 0.6, 0.8, 1], "sub", "Anteil Deletionen und Insertionen", 224)
    fig.legend((l1, l2, l3), ('Laufzeit', 'Gelöste Instanzen', 'Lösungsqualität'), 'lower right', bbox_to_anchor=[0.9, 0.1])
    plt.show()
    
    
    fig = plt.figure()
    problem = "median"
    something([1000, 2000, 3000, 4000, 5000, 10000], "length", "Länge der Grundsequenz", 221, [1000, 3000, 5000, 10000])
    something([20, 50, 100, 200, 300, 400, 500, 1000], "d", "Anzahl veränderter Positionen", 222, [0, 200, 500, 1000]) 
    something([5, 10, 20, 30, 40, 50], "n", "Anzahl der Sequenzen", 223, [5, 10, 20, 30, 40, 50])
    something([0, 0.2, 0.4, 0.6, 0.8, 1], "sub", "Anteil Deletionen und Insertionen", 224)
    fig.legend((l1, l2, l3), ('Laufzeit', 'Gelöste Instanzen', 'Lösungsqualität'), 'lower right', bbox_to_anchor=[0.9, 0.1])
    plt.show()