﻿import argparse
import os
import glob
import csv
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='path to folder with files to evaluate')
    args = parser.parse_args()
    os.chdir(args.path)
    
    deltas = [1, 2, 3, 4, 5, 10]
    qual_solutions = {}
    num_solved = {}
    num = 0
    for delta in deltas:
        qual_solutions[delta] = []
        num_solved[delta] = 0
    
    for f in glob.glob("*.fas"):
        if "eval_delta" not in f:
            continue
        num += 1
        fs = f[:-4] + "_solution.txt"
        with open(fs) as cf:
            reader = csv.reader(cf, delimiter=':')
            for row in reader:
                if row[0] == "optimal centre string d":
                    lower_bound = int(row[1])
        fd = f[:-4] + "_dist.csv"
        with open(fd) as cf:
            reader = csv.reader(cf, delimiter=':')
            for row in reader:
                if row[0] == "max_centre_d":
                    upper_bound = int(row[1])
        for delta in deltas:
            fa = f[:-4] + "_centre_delta" + str(delta) + "_string.csv"

            with open(fa) as cf:
                reader = csv.reader(cf, delimiter=';')
                for row in reader:
                    if row[0] == "solution":
                        num_solved[delta] += 1
                    if row[0] == "max_dist":
                        d = int(row[1])
                        q = (upper_bound - max(d, lower_bound)) / max(upper_bound - lower_bound, 1)
                        if upper_bound == lower_bound:
                            q = 1
                        qual_solutions[delta].append(q)
    
    yn = []
    yq = []
    xq = []
    for delta in deltas:
        yn.append(num_solved[delta] / num)
        if(len(qual_solutions[delta]) > 0):
            yq.append(sum(qual_solutions[delta]) / len(qual_solutions[delta]))
            xq.append(delta)
    
    fig, ax1 = plt.subplots()
    
    l2 = mlines.Line2D([], [], color='g', marker='o')
    l3 = mlines.Line2D([], [], color='b', marker='o')
    fig.legend((l2, l3), ('Gelöste Instanzen', 'Lösungsqualität'), 'lower right', bbox_to_anchor=[0.9, 0.1])
    
    ax1.plot(xq, yq, color="b", marker='o', ls='dotted', clip_on=False)
    ax1.plot(deltas, yn, color="g", marker='o', ls='dotted', clip_on=False)
    ax1.set_xlabel(r'Faktor $\Delta$')
    ax1.set_ylabel('Durchschnittliche Lösungsqualität\nAnteil gelöster Instanzen')
    
    plt.show()
    