import numpy as np
import itertools
import math
from collections import OrderedDict

def read_sequences(file_name):
    """
    Reads sequences from file
    file_name:  path to file from which to read sequences
    return each line as sequence from .txt files, or all seqeunces listed in a .fas file
    """
    with open(file_name, 'r') as f:
        lines = [line.strip() for line in f]
        if file_name[-4:] == ".fas":
            lines = [line for line in lines if line[0] != ">"]
        return lines
        
def levenshtein(source, target):
    """
    Calculates levenshtein distance between 2 strings
    source:     first string
    target:     second string
    return levenshtein distance between the two strings (Integer)
    """
    if len(source) < len(target):
        return levenshtein(target, source)

    if len(target) == 0:
        return len(source)
 
    source = np.array(tuple(source))
    target = np.array(tuple(target))
 
    previous_row = np.arange(target.size + 1)
    for s in source:
        current_row = previous_row + 1

        current_row[1:] = np.minimum(
                current_row[1:],
                np.add(previous_row[:-1], target != s))

        current_row[1:] = np.minimum(
                current_row[1:],
                current_row[0:-1] + 1)
 
        previous_row = current_row
 
    return previous_row[-1]
    
def levenshtein_weighted_score(char, char_list):
    """
    Calculate edit weight between a character and a list of characters from a weighted string
    char:       character of normal string
    char_list:  list of characters occuring in weighted string
    return edit weight from single char to list of chars
    """
    if len(char) > len(char_list):
        return levenshtein_approx_score(char_list, char)
    char = char[0]
    return 1 - char_list.count(char) / len(char_list)
    
def levenshtein_weighted(s1, s2):
    """
    Calculates levenshtein distance between a string and a weighted string
    s1:         normal string
    s2:         weighted string
    return levenshtein distance between the two strings (Float)
    """
    s1 = list(map(lambda c: [c], s1))

    if len(s2) == 0:
        return len(s1)
    if len(s1) == 0:
        return len(s2)
 
    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + levenshtein_weighted_score(c1, c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
 
    return previous_row[-1]
    
def get_alphabet(sequences):
    """
    calculates alphabet of given set of sequences
    sequences: set of sequences
    return list of all occuring characters of the used alphabet
    """
    return sorted(list(set([char for sequence in sequences for char in sequence])))
    
def validate_alphabet(sequences, alphabet):
    """
    checks whether alphabet matches given sequences
    sequences:  set of sequences to check
    alphabet:   alphabet to validate sequences over
    return true if only characters of given alphabet occur in the given sequences
    """
    return all(map(lambda x: len(set(x) - set(alphabet)) == 0, sequences))

def modify_string(str, char, operation):
    """
    modifies string by using given operation
    str:        string which to modify
    char:       character which to put into string
    operation:  operation to use ("sub", "ins" or "del")
    return tuple of modified string and modification cost
    """
    if operation == "sub":
        return (char + str[1:], 1)
    if operation == "ins":
        return (char + str, 1)
    if operation == "del":
        return (str[str.index(char):], str.index(char))
        
def calc_distance_values(sequences, remove_duplicates, verbose):
    """
    calculates info on maximum and minimum value for cost limits, and other helpful distance data
    sequences:          list of sequences to calculate info on
    remove_duplicates:  Boolean whether to remove duplicates from sequence list
    verbose:            Prints info on calculated values if set to true
    return dict with different infos on distances between sequences
    """
    #check for duplicates
    orig_num_sequences = len(sequences)
    sequence_set = set(sequences)
    num_duplicates = 0
    reduced_sequences = sequences
    if len(sequence_set) < len(sequences):
        num_duplicates = len(sequences) - len(sequence_set)
        if verbose:
            print(str(num_duplicates) + " duplicate sequences.")
        reduced_sequences = list(OrderedDict.fromkeys(sequences))
        if remove_duplicates:
            if verbose:
                print("duplicates removed.")
            sequences = reduced_sequences
            
    #initialize values for distance calculations
    indices = range(len(reduced_sequences))
    distances = [{} for i in range(len(sequences))]

    if verbose:
        print("calculating distances..")

    #calculate levenshtein distance for all not duplicated pairs of sequences
    for pair in itertools.combinations(indices, 2):
        s = reduced_sequences[pair[0]]
        t = reduced_sequences[pair[1]]
        d = levenshtein(s, t)
        
        #save distances for all copies of sequence
        indices_s = [i for i, x in enumerate(sequences) if x == s]
        indices_t = [i for i, x in enumerate(sequences) if x == t]
        for index_s in indices_s:
            for index_t in indices_t:
                distances[index_s][index_t] = d
                distances[index_t][index_s] = d
                
    #add null values to distance dicts
    for d in distances:
        for i in range(len(distances)):
            if i not in d:
                d[i] = 0

    #determine bounds for centre string instances
    max_centre_d = min(map(lambda x: max(x.values()), distances))
    min_centre_d = math.ceil(max(map(lambda x: max(x.values()), distances)) / 2)
    
    #determine bounds for median string instances
    max_median_d = min(map(lambda x: sum(x.values()), distances))
    arbitary_path_cost = sum(map(lambda i: distances[i][(i+1) % len(distances)], range(len(distances))))
    min_median_d = arbitary_path_cost // 2 
    
    #determine average distance of insider and outsider sequence to other sequences
    avg_min = round(max_median_d / (len(sequences) - 1), 1)
    avg_max = round(max(map(lambda x: sum(x.values()), distances)) / (len(sequences) - 1), 1)
    
    #determine average length of sequences and error rate
    sum_dist = sum(map(lambda x: sum(x.values()), distances)) / 2
    num_pairs = int((len(sequences) * (len(sequences)-1)) / 2)
    avg_dist = int(round(sum_dist / num_pairs))
    avg_length = int(round(sum(map(lambda s: len(s), sequences)) / len(sequences)))
    error_rate = round(avg_length / avg_dist, 1)
    return {
        "distances" : distances,
        "orig_num_sequences" : orig_num_sequences,
        "duplicates" : num_duplicates,
        "avg_len" : avg_length,
        "max_centre_d" : max_centre_d,
        "min_centre_d" : min_centre_d,
        "max_median_d" : max_median_d,
        "min_median_d" : min_median_d,
        "avg_dist_min" : avg_min,
        "avg_dist_max" : avg_max,
        "error_rate" : error_rate,
    }
