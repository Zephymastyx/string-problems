import functions
from abc import ABCMeta
from string_problem import Sequence, StringProblemInstance
from centre_string import CentreStringInstance
from median_string import MedianStringInstance
from name_info import name_exists, is_name

class NameProblemInstance(StringProblemInstance):
    __metaclass__ = ABCMeta
        
    def is_solved(self):
        return is_name(self.solution, self.options["gender"])
        
        
class CentreNameInstance(NameProblemInstance, CentreStringInstance):
    def filter_characters(self, chars):
        return [char for char in chars if len(list(filter(lambda s: s.string[0] != char and s.cost == self.cost_limit, self.sequences))) == 0 and name_exists(self.solution + char, self.options["gender"])]
        
    def is_solved(self):
        return CentreStringInstance.is_solved(self) and NameProblemInstance.is_solved(self)
    
class MedianNameInstance(NameProblemInstance, MedianStringInstance):
    def filter_characters(self, chars):
        return [char for char in chars if name_exists(self.solution + char, self.options["gender"])]
        
    def is_solved(self):
        return MedianStringInstance.is_solved(self) and NameProblemInstance.is_solved(self)
    