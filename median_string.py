import functions
from string_problem import Sequence, StringProblemInstance

class MedianStringInstance(StringProblemInstance):

    def get_allowed_cost(self, sequence):
        return self.get_remaining_costs()

    def is_solved(self):
        return self.get_remaining_costs() >= sum(map(lambda s: len(s.string), self.sequences)) - len(self.sequences)

    def is_unsolvable(self):
        return not self.is_solved() and self.get_remaining_costs() <= 0

    def filter_characters(self, chars):
        return chars
        
    def get_remaining_costs(self):
        """
        determine remaining allowed costs for current instance
        return number of remaining allowed costs
        """
        return self.cost_limit - sum(map(lambda s: s.cost, self.sequences))
